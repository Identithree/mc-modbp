// Suppress to fix IntelliJ bug.
@Suppress("DSL_SCOPE_VIOLATION")

// Plugin Definitions
plugins {
    id("maven-publish")
    alias(libs.plugins.quilt.loom)
    alias(libs.plugins.kotlin)
    alias(libs.plugins.dokka)
}

// Variable Assignment
group = project.properties["mavenGroup"]!!
version = project.version
val archivesBaseName = rootProject.properties["archivesBaseName"]!!
val licenseFileName = rootProject.properties["licenseFileName"]!! // If you plan to use a different file for the license, don't forget to change the file name here!

val javaVersion = 17 // Minecraft's default version of Java

// Repository Definitions
repositories {
    // Add repositories to retrieve artifacts from in here.
    // You should only use this when depending on other mods because
    // Loom adds the essential maven repositories to download Minecraft and libraries from automatically.
    // See https://docs.gradle.org/current/userguide/declaring_repositories.html
    // for more information about repositories.

    mavenCentral() // Default Maven repository + all required Minecraft repositories
    maven {
        url = uri("https://maven.shadew.net/") // Shadew's Maven repository (Used for an optional dependency)
    }
}

// Dependency Definitions
// All the dependencies are declared at gradle/libs.version.toml and referenced with "libs.<id>"
// See https://docs.gradle.org/current/userguide/platforms.html for information on how version catalogs work.
dependencies {
    // Minecraft
    minecraft(rootProject.libs.minecraft)
    mappings(loom.layered {
        addLayer(quiltMappings.mappings("org.quiltmc:quilt-mappings:${rootProject.libs.versions.quilt.mappings.get()}:v2")) // Quilt mappings. If below line is uncommented, it will be used as a fallback.
        // officialMojangMappings() // Uncomment to use the official Mojang mappings (Mojmaps).
    })

    // Quilt Mod Loader
    modImplementation(rootProject.libs.quilt.loader) // Quilt Loader
    modImplementation(rootProject.libs.quilt.api) // Quilt API; QSL is not complete, so a quilted version of the Fabric API is automatically included.
    modImplementation(rootProject.libs.bundles.qkl) // Quilt Kotlin Libraries

    // Kotlin
    implementation(rootProject.libs.kotlin.stdlib) // Kotlin + Standard Library
    // dokkaHtmlPlugin(rootProject.libs.dokka.html) // Uncomment if you want to use Dokka to generate HTML documentation with KDoc.

    // Other Libraries
    // implementation(rootProject.libs.shwutil) // Uncomment this line if you want to use ShwUtil by FoxShadew (https://github.com/FoxShadew/ShwUtil).
}

tasks {
    processResources {
        inputs.property("version", version)
        filesMatching("quilt.mod.json") {
            expand(mapOf("version" to version))
        }
    }

    withType(JavaCompile::class.java).configureEach {
        options.encoding = "UTF-8"
        options.release.set(17) // Minecraft 1.18 (1.18-pre2) and higher use Java 17.
    }

    jar {
        from(licenseFileName) {
            rename {
                "${it}_${archivesBaseName}"
            }
        }
    }

    publishing {
        publications {
            create<MavenPublication>("mavenJava") {
                artifact(remapJar) {
                    builtBy(remapJar)
                }
            }
        }

        // See https://docs.gradle.org/current/userguide/publishing_maven.html for information on how to set up publishing.
        repositories {
            // Add repositories to publish to here.
            // Notice: This block does NOT have the same function as the block in the top level.
            // The repositories here will be used for publishing your artifact, not for
            // retrieving dependencies.
        }
    }

    // Set Kotlin to use Java 17 as a compilation target
    compileKotlin {
        kotlinOptions.jvmTarget = "17"
    }

    compileTestKotlin {
        kotlinOptions.jvmTarget = "17"
    }
}

java {
    // Required by IDEs like Eclipse and Visual Studio Code
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17

    // Loom will automatically attach sourcesJar to a RemapSourcesJar task and to the "build" task if it is present.
    // If you remove this line, sources will not be generated.
    withSourcesJar()

    // If this mod is going to be a library, then it should also generate Javadocs in order to aid with developement.
    // Uncomment this line to generate them.
    // withJavadocJar()
}
